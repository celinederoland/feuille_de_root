-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 04 Décembre 2015 à 01:15
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `nuitdelinfo`
--

-- --------------------------------------------------------

--
-- Structure de la table `competences`
--

CREATE TABLE `competences` (
  `idcompetences` int(11) NOT NULL,
  `nomcompetence` varchar(45) NOT NULL,
  `membre_idmembre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Dons`
--

CREATE TABLE `Dons` (
  `idDons` int(11) NOT NULL,
  `Montant` float DEFAULT NULL,
  `membre_idmembre` int(11) DEFAULT NULL,
  `horairedebutdon` datetime DEFAULT NULL,
  `horairefindon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `geolocalisation`
--

CREATE TABLE `geolocalisation` (
  `idgeolocalisation` int(11) NOT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `dateheuregeoloc` datetime DEFAULT NULL,
  `appeldetresse` tinyint(1) DEFAULT NULL,
  `membre_idmembre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `geste_preventif`
--

CREATE TABLE `geste_preventif` (
  `idgeste_preventif` int(11) NOT NULL,
  `nomgeste` varchar(80) DEFAULT NULL,
  `explicationgeste` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE `membre` (
  `idmembre` int(11) NOT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `adresse` varchar(60) DEFAULT NULL,
  `password` varchar(45) NOT NULL,
  `jeton` varchar(15) NOT NULL,
  `CP` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `protectioncivile`
--

CREATE TABLE `protectioncivile` (
  `montant` int(11) DEFAULT NULL,
  `nombre_menbre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

CREATE TABLE `services` (
  `horairedebut` datetime DEFAULT NULL,
  `horairefin` datetime DEFAULT NULL,
  `journee` int(11) DEFAULT NULL,
  `idservice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `services_has_membre`
--

CREATE TABLE `services_has_membre` (
  `services_idservice` int(11) NOT NULL,
  `membre_idmembre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `competences`
--
ALTER TABLE `competences`
  ADD PRIMARY KEY (`idcompetences`),
  ADD UNIQUE KEY `nomcompetence_UNIQUE` (`nomcompetence`),
  ADD KEY `fk_competences_membre1_idx` (`membre_idmembre`);

--
-- Index pour la table `Dons`
--
ALTER TABLE `Dons`
  ADD PRIMARY KEY (`idDons`),
  ADD KEY `fk_Dons_membre1_idx` (`membre_idmembre`);

--
-- Index pour la table `geolocalisation`
--
ALTER TABLE `geolocalisation`
  ADD PRIMARY KEY (`idgeolocalisation`),
  ADD KEY `fk_geolocalisation_membre1_idx` (`membre_idmembre`);

--
-- Index pour la table `geste_preventif`
--
ALTER TABLE `geste_preventif`
  ADD PRIMARY KEY (`idgeste_preventif`);

--
-- Index pour la table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`idmembre`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `jeton_UNIQUE` (`jeton`);

--
-- Index pour la table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`idservice`);

--
-- Index pour la table `services_has_membre`
--
ALTER TABLE `services_has_membre`
  ADD PRIMARY KEY (`services_idservice`,`membre_idmembre`),
  ADD KEY `fk_services_has_membre_membre1_idx` (`membre_idmembre`),
  ADD KEY `fk_services_has_membre_services1_idx` (`services_idservice`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `membre`
--
ALTER TABLE `membre`
  MODIFY `idmembre` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `competences`
--
ALTER TABLE `competences`
  ADD CONSTRAINT `fk_competences_membre1` FOREIGN KEY (`membre_idmembre`) REFERENCES `membre` (`idmembre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Dons`
--
ALTER TABLE `Dons`
  ADD CONSTRAINT `fk_Dons_membre1` FOREIGN KEY (`membre_idmembre`) REFERENCES `membre` (`idmembre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `geolocalisation`
--
ALTER TABLE `geolocalisation`
  ADD CONSTRAINT `fk_geolocalisation_membre1` FOREIGN KEY (`membre_idmembre`) REFERENCES `membre` (`idmembre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `services_has_membre`
--
ALTER TABLE `services_has_membre`
  ADD CONSTRAINT `fk_services_has_membre_services1` FOREIGN KEY (`services_idservice`) REFERENCES `services` (`idservice`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_services_has_membre_membre1` FOREIGN KEY (`membre_idmembre`) REFERENCES `membre` (`idmembre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

