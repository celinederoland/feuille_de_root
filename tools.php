<?php

function postMembre ($bdd, $email, $password, $admin, $adresse, $jeton, $cp) {
	$bdd->beginTransaction();
	$myData = array(':email'=>$email, ':password'=>$password, ':admin'=>$admin, ':adresse'=>$adresse, ':jeton'=>$jeton, ':cp'=>$cp);
	$que = $bdd->prepare('INSERT INTO membre (email, password, admin, adresse, jeton, cp) VALUES (:email, :password, :admin, :adresse, :jeton, :cp)');
	$que->bindValue(":admin", $admin, PDO::PARAM_INT);
	$que->execute($myData);
	$bdd->commit();

	$que = $bdd->prepare("SELECT * FROM membre WHERE email=:email");
	$que->execute(array(':email'=>$myData[':email']));
	echo json_encode($que->fetchAll(PDO::FETCH_ASSOC));
}

function postGeo ($bdd, $longitude, $latitude, $idmembre=null) {
	$bdd->beginTransaction();

	if ($idmembre != null) {
		$que = $bdd->prepare('INSERT INTO geolocalisation (longitude, latitude, dateheuregeoloc, appeldetresse, membre_idmembre) VALUES (:longitude, :latitude, NOW(), 1, :idmembre)');
		$que->bindValue(":idmembre", $idmembre, PDO::PARAM_INT);
	} else {
		$que = $bdd->prepare('INSERT INTO geolocalisation (longitude, latitude, dateheuregeoloc, appeldetresse) VALUES 		(:longitude, :latitude, NOW(), 1)');
	}

	$que->bindValue(":longitude", $longitude);
	$que->bindValue(":latitude", $latitude);
	$que->execute();
	$bdd->commit();



	$que = $bdd->prepare("SELECT * FROM membre WHERE idgeolocalisation=:id");
	echo $bdd->lastInsertId();
	$que->bindValue(':id', $bdd->lastInsertId(), PDO::PARAM_INT);
	$que->execute();
	echo json_encode($que->fetchAll(PDO::FETCH_ASSOC));
}


function get ($table, $bdd, $id=null) {
	$que = null;	
	if ($id != null) {
		$que = $bdd->prepare("SELECT * FROM ".$table." WHERE id".$table."=:id");
		$que->bindValue(":id", $id, PDO::PARAM_INT);
	} else {
		$que = $bdd->prepare("SELECT * FROM ".$table);
	}
	$que->execute();
	return json_encode($que->fetchAll(PDO::FETCH_ASSOC));
}
