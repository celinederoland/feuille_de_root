<?php
header("Access-Control-Allow-Origin:*");

#php settings
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$bdd = new PDO('mysql:host=sleek-think.ovh;dbname=nuitinfo;charset=utf8', 'toto', 'toto');
include_once "tools.php";

switch($_SERVER['REQUEST_METHOD']) {
	case 'POST' :
		if (isset($_REQUEST['membre_idmembre']))
			echo postGeo($bdd, $_REQUEST['longitude'], $_REQUEST['latitude'], $_REQUEST['membre_idmembre']);
		else
			echo postGeo($bdd, $_REQUEST['longitude'], $_REQUEST['latitude']);
		break;
	case 'GET' :
		if (isset($_GET['id']))
			echo get("geolocalisation", $bdd, $_GET['id']);
		else
			echo get("geolocalisation", $bdd);
		break;
}
