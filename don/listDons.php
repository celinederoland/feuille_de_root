﻿<?php
include_once "db.php";
?>
<!DOCTYPE html> 
<html>
<head>
	<meta charset="UTF-8">
	<title>liste dons</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css" />
</head>
<body>
	<header>
		<a href='listDons.php' id='title'>visualiser dons</a>
		<a href='don.php' id='title'>faire un don</a>
		<a href='..' id='title'>Accueil</a>
	</header>
        <p>Don total: <?php
			$listedons = findAllDons();
			$total = 0;
			foreach($listedons as $don)
			{
				if($don["horairedebutdon"]==null)
				{
					$total += $don["Montant"];
				}
				else if($don["horairefindon"]==null)
				{
					$datedebut = new datetime($don["horairedebutdon"]);
					$datefin = new datetime(date('Y-m-d'));
					$difference = $datefin->diff($datedebut);
					$total += $don["Montant"] * $difference->m;
				}
				else
				{
					$datedebut = new datetime($don["horairedebutdon"]);
					$datefin = new datetime($don["horairefindon"]);
					$difference = $datefin->diff($datedebut);
					$total += $don["Montant"] * $difference->m;
				}
			}
			echo $total."€";
			
		?></p>
		
        <h2>Listes des dons : </h2>
			<table border="1" border-collapse="collapse">
				<th>Montant</th><th>datedebut</th><th>datefin</th>
				<?php
				foreach($listedons as $don)
				{
					echo "<tr>";
					?>
					<td><?php echo $don["Montant"]."€"; ?></td><td><?php echo $don["horairedebutdon"]; ?></td><td><?php echo $don["horairefindon"]; ?></td>
					<?php
					echo "</tr>";
				}
			?>
			</table>
		<script src="jquery-1.11.3.js"></script>
		<script src="formDons.js"></script>
        </body>
</html>



<?php

function findAllDons(){
	$listedons = array();
	$st = db()->prepare("select * from dons");
	$st->execute();
	while($row = $st->fetch(PDO::FETCH_ASSOC)){
		$don=$row;
		array_push($listedons, $don);
	}
	return $listedons;
}