<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$bdd = new PDO('mysql:host=sleek-think.ovh;dbname=nuitinfo;charset=utf8', 'toto', 'toto');
include_once "tools.php";


if($_SERVER['REQUEST_METHOD'] == "POST") {

	//Récupérer le jeton et trouver le user à qui il appartient
	$que = $bdd->prepare("SELECT * FROM membre WHERE jeton=:jeton");
	$que->bindValue(":jeton", $_REQUEST['jeton']);
	$que->execute();
	if ($user = $que->fetch(PDO::FETCH_ASSOC)) {
	
	
		//On cherche si le user a déjà un service
		$que = $bdd->prepare("SELECT * FROM services WHERE membre_idmembre=:idmembre");
		$que->bindValue(":idmembre", $user['idmembre']);
		$que->execute();

		$bdd->beginTransaction();
		//Si le user a déjà un service on l'udate
		if($service = $que->fetch(PDO::FETCH_ASSOC)) {
			$que = $bdd->prepare("UPDATE services SET horairedebut=:horairedebut, horairefin=:horairefin, journee=:journee WHERE membre_idmembre=:idmembre");
		
		}
		//Sinon on le crée
		else {
			$que = $bdd->prepare("INSERT INTO services (horairedebut, horairefin, journee, membre_idmembre) VALUES(:horairedebut, :horairefin, :journee, :idmembre) ");
		}
		$que->bindValue(":idmembre", $user['idmembre']);
		$que->bindValue(":horairedebut", $_REQUEST['horairedebut']);
		$que->bindValue(":horairefin", $_REQUEST['horairefin']);
		$que->bindValue(":journee", $_REQUEST['journee']);
		$que->execute();
		$bdd->commit();

		//Je rends le service
		$que = $bdd->prepare("SELECT * FROM services WHERE membre_idmembre=:idmembre");
		$que->bindValue(":idmembre", $user['idmembre']);
		$que->execute();
		$service = $que->fetch(PDO::FETCH_ASSOC);
		echo json_encode($service);
	} else {
		echo json_encode(array('error'=>true));
	}
} else if ($_SERVER['REQUEST_METHOD'] == "GET") {

	if (isset($_GET['id'])) {
		echo get("services", $bdd, $_GET['id']);
	} else {
		echo get("services", $bdd);
	}
}
?>
